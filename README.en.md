# 怎样给微信公众号活跃粉丝推送未发布的文章链接

#### Description
为了满足微信公众号给活跃粉丝推送未发布的文章链接，第三方平台微号帮提供了48小时信息推送功能实现，可以给公众号活跃粉丝推送文章链接，因为未发布的文章链接为临时链接，需要再通过微号帮功能获取素材永久链接去得到未发布文章的永久链接，然后设置在公众号48小时活跃粉丝推送消息里...

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
